import React from 'react'
import Header from './Header'
import ProductList from './ProductList'
import Aside from './Aside'
import Footer from './Footer'

const Home = () => {
    return (
        <>
            <Header/>
            <Aside/>
            <ProductList/>
            <Footer/>
        </>
    )
}

export default Home