import React from 'react'

const Footer = () => {
    return (  
        <footer className="footer">
            <nav className="footer__menu">
                <ul className="footer__menu-list">
                    <li className="footer__menu-item">
                        <a href="#ayuda" className="footer__menu-link">Ayuda</a> 
                    </li>
                    <li className="footer__menu-item">
                        <a href="#contacto" className="footer__menu-link">Contacto</a>
                    </li>
                    <li className="footer__menu-item">
                        <a href="#condiciones" className="footer__menu-link">Términos y condiciones</a>
                    </li>
                    <li className="footer__menu-item">
                        <a href="#privacidad" className="footer__menu-link">Política de privacidad</a>
                    </li>
                </ul>
            </nav>
        </footer>
    );
}
 
export default Footer;