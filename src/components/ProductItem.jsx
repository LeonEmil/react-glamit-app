import React from 'react';

const ProductList = ({title, price, src, alt}) => {
    return (  
        <div className="product-item__container">            
            <picture className="product-item__img-container">
                <img
                    className="product-item__img"
                    src={src}
                    alt={alt}
                />
            </picture>
            <h3 className="product-item__name">{title}</h3>
            <span className="product-item__price">${price}</span>
        </div>
    );
}
 
export default ProductList;