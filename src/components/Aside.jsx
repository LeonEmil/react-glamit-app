import React from 'react'

const Aside = () => {
    return (
        <div className="aside">
            <h2 className="aside__title">Zapatillas</h2>
            <h2 className="aside__title">Filtrar</h2>
            <h3 className="aside__subtitle">Categoría</h3>
                <ul className="aside__list">
                    <li className="aside__list-item">Zapatillas</li>
                    <li className="aside__list-item">Zapatos</li>
                    <li className="aside__list-item">Botas</li>
                    <li className="aside__list-item">Sandalias</li>
                    <li className="aside__list-item">Pantuflas</li>
                </ul>
            <h3 className="aside__subtitle">Talle</h3>
                <ul className="aside__list">
                    <li className="aside__list-item">36</li>
                    <li className="aside__list-item">38</li>
                    <li className="aside__list-item">40</li>
                    <li className="aside__list-item">42</li>
                    <li className="aside__list-item">44</li>
                </ul>
            <h3 className="aside__subtitle">Color</h3>
                <ul className="aside__list">
                    <li className="aside__list-item">Amarillo</li>
                    <li className="aside__list-item">Negro</li>
                    <li className="aside__list-item">Verde</li>
                    <li className="aside__list-item">Azul</li>
                    <li className="aside__list-item">Blanco</li>
                </ul>
        </div>
    )
}

export default Aside