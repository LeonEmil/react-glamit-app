import React, { useState, useEffect } from 'react';
import axios from 'axios'
import ProductItem from './ProductItem'
import ClipLoader from 'react-spinners/ClipLoader'

const ProductList = () => {

    const [products, setProducts] = useState([])
    
    useEffect(() => {
        axios.get('https://my-json-server.typicode.com/LeonEmil/glamit-app/products').then(response => {
            console.log(response)
            setProducts(response.data)
        })
    })

    return products.length > 0 ? (  
        <main className="product-list">
            {
                products.map(product => {
                    return <ProductItem 
                                title={product.title}
                                price={product.price}
                                src={product.src}
                                alt={product.alt}
                           />
                })
            }
        </main>
    ) : 
    (
        <div className="loader">
            <ClipLoader />
        </div>
    )
}
 
export default ProductList;