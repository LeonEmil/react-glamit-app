import React, { useState } from 'react';
import logo from './../logo.svg'

const Header = () => {

    const [showPanel, setShowPanel] = useState(false)

    const handlePanel = () => {
        if(showPanel) setShowPanel(false) 
        else setShowPanel(true)
    }

    return (  
        <header className="header">
            <div className="menu-icon" onClick={handlePanel}></div>
            <img src={logo} alt="La preuve logo" className="logo"/>
            <nav className="menu">
                <ul className="menu__list">
                    <li className="menu__item"><a href="#sale" className="menu__link">Sale</a></li>
                    <li className="menu__item"><a href="#eshop" className="menu__link">E-shop</a></li>
                    <li className="menu__item"><a href="#lookbook" className="menu__link">Lookbook</a></li>
                    <li className="menu__item"><a href="#campaña" className="menu__link">Campaña</a></li>
                    <li className="menu__item"><a href="#locales" className="menu__link">Locales</a></li>
                </ul>
            </nav>
            <ul className="menu__icons">
                <li className="search-icon"></li>
                <li className="user-icon"></li>
                <li className="shop-icon"></li>
            </ul>
            <div className="menu__panel" style={showPanel ? {left: `0`} : {left: `-300px`}}>
                <div className="menu__panel-cross" onClick={handlePanel}></div>
                <ul className="menu__panel-list">
                    <li className="menu__panel-item"><a href="#sale" className="menu__panel-link">Sale</a></li>
                    <li className="menu__panel-item"><a href="#eshop" className="menu__panel-link">E-shop</a></li>
                    <li className="menu__panel-item"><a href="#lookbook" className="menu__panel-link">Lookbook</a></li>
                    <li className="menu__panel-item"><a href="#campaña" className="menu__panel-link">Campaña</a></li>
                    <li className="menu__panel-item"><a href="#locales" className="menu__panel-link">Locales</a></li>
                </ul>
            </div>
        </header>
    );
}
 
export default Header;